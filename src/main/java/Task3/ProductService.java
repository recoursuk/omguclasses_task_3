package Task3;

import Task1.*;
import Task2.BeginStringFilter;
import Task2.Filter;

public class ProductService {

    public static int countByFilter(Filter filter, Consignment consignment){
        int counter = 0;
        for(PackedProduct product: consignment.getProducts()){
            if(product instanceof Product) {
                Product currentProduct = (Product) product;
                if(filter.apply(currentProduct.getName())){
                    counter++;
                }
            }
        }
        return counter;
    }

    public static int countByFilterDeep(Filter filter, Consignment consignment){
        int counter = 0;
        for(PackedProduct product: consignment.getProducts()){
            if(product instanceof PackagedSetProduct){
                counter+= countByFilterDeep(filter, new Consignment((PackagedSetProduct) product,"description"));
            }

            if(product instanceof Product) {
                Product currentProduct = (Product) product;
                if(filter.apply(currentProduct.getName())){
                    counter++;
                }
            }
        }
        return counter;
    }

    public static boolean checkAllWeighted(Consignment consignment){
        for(PackedProduct product: consignment.getProducts()){
            if(!(product instanceof WeightProduct)){
                return false;
            }
        }
        return true;
    }
}
