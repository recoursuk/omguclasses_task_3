package Task1;

import java.util.Objects;

public class PackagedWeightProduct extends WeightProduct implements PackedProduct{
    private Pack pack;
    private double weight;
    public PackagedWeightProduct(WeightProduct product, double weight, Pack pack) {
        super(product);
        if (product == null || pack == null || weight < 0) {
            throw new IllegalArgumentException("Illegal argument");
        } else {
            this.weight = weight;
            this.pack = pack;
        }
    }
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight < 0) {
            throw new IllegalArgumentException("Weight should be positive");
        } else {
            this.weight = weight;
        }
    }
    @Override
    public double getNetWeight(){
        return weight;
    }

    @Override
    public double getGrossWeight(){
        return weight+ pack.getWeight();
    }

    public Pack getPack() {
        return pack;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    @Override
    public String toString() {
        return "Task1.PackagedWeightProduct{" +
                "consignment=" + pack +
                ", weight=" + weight +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedWeightProduct that = (PackagedWeightProduct) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(pack, that.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pack, weight);
    }
}
