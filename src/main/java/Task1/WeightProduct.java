package Task1;

public class WeightProduct extends Product{
    public WeightProduct(String name, String description){
        super(name, description);
    }

    public WeightProduct(WeightProduct product){
        super(product);
    }



    @Override
    public String toString() {
        return "Task1.WeightProduct{} " + super.toString();
    }
}
