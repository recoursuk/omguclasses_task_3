package Task1;

public interface PackedProduct {
    double getNetWeight();
    double getGrossWeight();
}
