package Task1;

import java.util.Objects;

public class PieceProduct extends Product {
    private double weight;
    public PieceProduct(String name, String description, double weight){
        super(name, description);
        this.weight = weight;
    }

    public PieceProduct(PieceProduct product){
        super(product);
        this.weight = product.getWeight();
    }

    public double getWeight(){
        return weight;
    }

    public void setWeight(int weight){
        if (weight < 0){
            throw new IllegalArgumentException("Weight can't be null");
        }
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Task1.PieceProduct{" +
                "weight=" + weight +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PieceProduct that = (PieceProduct) o;
        return Double.compare(that.weight, weight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight);
    }
}
