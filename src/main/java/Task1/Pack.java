package Task1;

import java.util.Objects;

public class Pack {
    private String name;
    private double weight;

    public Pack(String name, double weight) {
        if (name == null || weight < 0) {
            throw new IllegalArgumentException("Illegal args");
        } else{
            this.name = name;
            this.weight = weight;
        }
    }
    public String getName(){
        return name;
    }

    public double getWeight(){ return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight < 0) {
            throw new IllegalArgumentException("Wight can't be less than 0");
        } else {
            this.weight = weight;
        }
    }
    @Override
    public String toString() {
        return "Task1.Consignment{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pack that = (Pack) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight);
    }
}
