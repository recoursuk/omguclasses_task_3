package Task1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Consignment{
    private String description;
    private List<PackedProduct> products;

    public Consignment(PackedProduct[] products, String description){
        this.products = new LinkedList<>(Arrays.asList(products));
        if (description == null){
            throw new NullPointerException("Description can't be null");
        }
        this.description = description;
    }

    public Consignment(PackagedSetProduct packagedSetProduct, String description){

        products = packagedSetProduct.getProducts();
        this.description = description;
    }

    public List<PackedProduct> getProducts(){
        return products;
    }

    public double getWeight(){
        return products.stream().mapToDouble(PackedProduct::getGrossWeight).sum();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task1.Consignment{" +
                "description='" + description + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Consignment that = (Consignment) o;
        return Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), description);
    }
}
