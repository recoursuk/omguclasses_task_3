package Task1;

import java.util.Objects;

public class PackagedPieceProduct extends PieceProduct implements PackedProduct{
    private Pack pack;
    private int count;
    public PackagedPieceProduct(PieceProduct pieceProduct, int count, Pack pack) {
        super(pieceProduct);
        if (count < 0 ){
            throw new IllegalArgumentException("Count should be positive");
        } else {
            this.count = count;
        }
        if (pack == null) {
            throw new NullPointerException("Pack can't be null");
        } else {
            this.pack = pack;
        }
    }
    public int getQuantity() {
        return count;
    }

    public Pack getPack(){
        return pack;
    }

    public void setQuantity(int count){
        this.count = count;
    }

    public void setPack(Pack pack){
        if (pack == null){
            throw new NullPointerException("Pack can't be null");
            } else {
                this.pack = pack;
            }
        }


    @Override
    public double getNetWeight(){
        return count * super.getWeight();
    }

    @Override
    public double getGrossWeight(){
        return count * super.getWeight() + pack.getWeight();
    }

    @Override
    public String toString() {
        return "Task1.PackagedPieceProduct{" +
                "pack=" + pack +
                ", count=" + count +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedPieceProduct that = (PackagedPieceProduct) o;
        return count == that.count &&
                Objects.equals(pack, that.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pack, count);
    }
}
