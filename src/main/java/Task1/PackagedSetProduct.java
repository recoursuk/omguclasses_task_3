package Task1;

import java.util.*;

public class PackagedSetProduct implements PackedProduct {
    private Pack pack;
    private List<PackedProduct> products;

    public PackagedSetProduct(PackedProduct[] products, Pack pack) {
        this.products = new LinkedList<>(Arrays.asList(products));
        if (pack == null) {
            throw new IllegalArgumentException("Illegal args");
        }
            setPack(pack);
        }

    @Override
    public double getNetWeight(){
        double sum = 0;
        for (PackedProduct product: products){
            sum += product.getGrossWeight();
        }
        return sum;
    }

    @Override
    public double getGrossWeight(){
        return getNetWeight()+pack.getWeight();
    }

    public void setPack(Pack pack) {
        if(pack == null){
            throw new NullPointerException("Pack can't be null");
        } else {
            this.pack = pack;
        }
    }

    public Pack getPack(){
        return pack;
    }

    public void setProducts(List<PackedProduct> products) {
        this.products = products;
    }

    public List<PackedProduct> getProducts(){
        return products;
    }

    @Override
    public String toString() {
        return "Task1.PackagedSetProduct{" +
                "pack=" + pack +
                ", products=" + products +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackagedSetProduct that = (PackagedSetProduct) o;
        return Objects.equals(pack, that.pack) &&
                Objects.equals(products, that.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pack, products);
    }
}
