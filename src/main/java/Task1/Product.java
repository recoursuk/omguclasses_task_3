package Task1;

import java.util.Objects;

public class Product {
    private String name, description;

    public Product(String name, String description) {
        if (name == null || description == null) {
            throw new NullPointerException("String's can't be null");
        } else {
            this.name = name;
            this.description = description;
        }
    }
    public Product(Product product){
        this(product.getName(), product.getDescription());
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        if (this.name == null) {
            throw new NullPointerException("Null Pointer");
        } else {
            this.name = name;
        }
    }

    public String getDescription() {
        if (this.description == null) {
            throw new NullPointerException("Null Pointer");
        } else {
            return description;
        }
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Task1.Product{");
        sb.append("name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }
}
