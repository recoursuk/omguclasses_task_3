package Tests.Task1.Products;

import Task1.PieceProduct;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PieceProductTest {
    @Test
    public void Weight(){
        PieceProduct p = new PieceProduct("name","description",2);
        Assert.assertEquals(p.getWeight(),2,0.1);
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> p.setWeight(-2)));
        p.setWeight(3);
        Assert.assertEquals(p.getWeight(),3, 0.1);
    }
}