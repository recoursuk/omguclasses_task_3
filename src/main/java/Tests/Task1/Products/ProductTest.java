package Tests.Task1.Products;

import Task1.Product;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductTest {
    @Test
    public void Constructor(){
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> new Product(null,null)));
    }

    @Test
    public void GettersAndSetters(){
        Product p = new Product("name","description");
        Assert.assertEquals(p.getName(),"name");
        Assert.assertEquals(p.getDescription(),"description");
        p.setName("new name");
        p.setDescription("new description");
        Assert.assertEquals(p.getName(),"new name");
        Assert.assertEquals(p.getDescription(),"new description");
    }

    @Test
    public void Equals(){
        Product p1 = new Product("name","description");
        Product p2 = new Product("name","description");
        Product p3 = new Product("not similar name","description");
        Product p4 = new Product("name","not similar description");
        Assert.assertEquals(p1, p2);
        Assert.assertNotEquals(p1,p3);
        Assert.assertNotEquals(p1,p4);
    }
}
