package Tests.Task1.PackagedSetProduct;


import Task1.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ConsignmentTest {
    private static PackedProduct[] packedProducts = new PackedProduct[]{new PackagedPieceProduct(
            new PieceProduct("name 1","description 1",5),5,new Pack("name1",5))
            ,new PackagedWeightProduct(new WeightProduct("name 2","description 2"),5,new Pack("name1",5))
            ,new PackagedWeightProduct(new WeightProduct("name 3","description 3"),5,new Pack("name1",5))
            ,new PackagedWeightProduct(new WeightProduct("name 4","description 4"),5,new Pack("name1",5))
            ,new PackagedWeightProduct(new WeightProduct("name 5","description 5"),5,new Pack("name1",5))};

    private static Consignment packedProductsBach = new Consignment(packedProducts,"description");

    private static PackagedSetProduct PackagedSetProduct = new PackagedSetProduct(packedProducts,new Pack("name",5));

    private static Consignment pb = new Consignment(new PackedProduct[]{PackagedSetProduct,PackagedSetProduct},"description 2");
    @Test
    public void constructor(){
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> new Consignment(packedProducts,null)));
    }

    @Test
    public void weight(){
        Assert.assertEquals(packedProductsBach.getWeight(),70, 0.1);
        Assert.assertEquals(pb.getWeight(),150, 0.1);
    }
}
