package Tests.Task1.PackagedSetProduct;

import Task1.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PackagedSetProductTest {
    private  PackedProduct[] packedProducts = new PackedProduct[]{
            new PackagedPieceProduct(new PieceProduct("name 1","description 1",5),5,new Pack("name 1", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 2","description 2"),5,new Pack("name 2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 3","description 3"),5,new Pack("name 3", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 4","description 4"),5,new Pack("name 4", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 5","description 5"),5,new Pack("name 5", 5))};

    @Test
    public void constructor(){
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new PackagedSetProduct(packedProducts,(Pack) null)));
    }

    @Test
    public void weight(){
        PackedProduct p = new PackagedSetProduct(packedProducts,new Pack("name",5));
        Assert.assertEquals(p.getNetWeight(),70, 0.1);
        Assert.assertEquals(p.getGrossWeight(),75, 0.1);
    }

    @Test
    public void gettersAndSetters(){
        PackagedSetProduct p = new PackagedSetProduct(packedProducts,new Pack("name",5));
        Assert.assertEquals(p.getPack(),new Pack("name",5));
        p.setPack(new Pack("name 2",3));
        Assert.assertEquals(p.getPack(),new Pack("name 2",3));
        Assert.assertEquals(p.getProducts(),new LinkedList<>(Arrays.asList(packedProducts)));
        p.setProducts(new LinkedList<>(Arrays.asList(Arrays.copyOfRange(packedProducts,1,packedProducts.length))));
        Assert.assertEquals(p.getProducts(),new LinkedList<>(Arrays.asList(Arrays.copyOfRange(packedProducts,1,packedProducts.length))));
        p.setProducts(null);
        Assert.assertNull(p.getProducts());
    }
}
