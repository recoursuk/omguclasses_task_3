package Tests.Task1.PackagedProducts;


import Task1.Pack;
import Task1.PackagedWeightProduct;
import Task1.PackedProduct;
import Task1.WeightProduct;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PackagedWeightProductTest {
    @Test
    public void constructor(){
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new PackagedWeightProduct(new WeightProduct("name","description"),5,(Pack) null)));
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new PackagedWeightProduct(new WeightProduct("name","description"),-5,new Pack("name", 6))));
    }

    @Test
    public void weight(){
        PackedProduct p = new PackagedWeightProduct(new WeightProduct("name","description"),5, new Pack("pack name",5));
        Assert.assertEquals(p.getGrossWeight(),10, 0.1);
        Assert.assertEquals(p.getNetWeight(),5, 0.1);
    }

    @Test
    public void gettersAndSetters(){
        PackagedWeightProduct p = new PackagedWeightProduct(new WeightProduct("name", "description"), 5,new Pack("name",5));
        Assert.assertEquals(p.getWeight(),5, 0.1);
        p.setWeight(2);
        Assert.assertEquals(p.getWeight(),2, 0.1);

        Assert.assertEquals(p.getPack(),new Pack("name", 5));
        p.setPack(new Pack("name",3));
        Assert.assertEquals(p.getPack(),new Pack("name",3));
    }

    @Test
    public void equals(){
        PackagedWeightProduct p1 = new PackagedWeightProduct(new WeightProduct("name", "description"), 5,new Pack("name", 5));
        PackagedWeightProduct p2 = new PackagedWeightProduct(new WeightProduct("name", "description"), 6,new Pack("name", 5));
        PackagedWeightProduct p3 = new PackagedWeightProduct(new WeightProduct("name", "description"), 5,new Pack("name", 6));
        PackagedWeightProduct p4 = new PackagedWeightProduct(new WeightProduct("not the same name", "description"), 5,new Pack("name", 5));
        PackagedWeightProduct p5 = new PackagedWeightProduct(new WeightProduct("name", "not the same description"), 5,new Pack("name", 5));
        Assert.assertNotEquals(p1,p2);
        Assert.assertNotEquals(p1,p2);
        Assert.assertNotEquals(p1,p3);
        Assert.assertNotEquals(p1,p4);
        Assert.assertNotEquals(p1,(PackagedWeightProduct)null);
    }
}
