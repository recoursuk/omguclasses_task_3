package Tests.Task1.PackagedProducts;

import Task1.Pack;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PackTest {
    @Test
    public void Constructor(){
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new Pack("name", -2)));
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new Pack(null,2)));
    }

    @Test
    public void GettersAndSetters(){
        Pack p = new Pack("name", 5);
        Assert.assertEquals(p.getWeight(),5,0.1);
        p.setWeight(2);
        Assert.assertEquals(p.getWeight(),2, 0.1);
        Assert.assertEquals(p.getName(),"name");
        p.setName("new name");
        Assert.assertEquals(p.getName(),"new name");
    }

    @Test
    public void Equals(){
        Pack p1 = new Pack("name",3);
        Pack p2 = new Pack("name",3);
        Pack p3 = new Pack("not the same name",3);
        Pack p4 = new Pack("name",4);
        Assert.assertEquals(p1,p2);
        Assert.assertNotEquals(p1,p3);
        Assert.assertNotEquals(p1,p4);
        Assert.assertNotEquals(p1,(Pack) null);
    }
}