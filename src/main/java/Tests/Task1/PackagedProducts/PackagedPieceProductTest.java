package Tests.Task1.PackagedProducts;

import Task1.Pack;
import Task1.PackagedPieceProduct;
import Task1.PackedProduct;
import Task1.PieceProduct;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PackagedPieceProductTest {
    @Test
    public void constructor(){
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> new PackagedPieceProduct(new PieceProduct("name","description",5),5,(Pack) null)));
        Assert.assertNotNull(assertThrows(IllegalArgumentException.class,() -> new PackagedPieceProduct(new PieceProduct("name","description",5),-5,new Pack("name", 6))));
    }

    @Test
    public void weight(){
        PackedProduct p = new PackagedPieceProduct(new PieceProduct("name","description",5),5,new Pack("name",5));
        Assert.assertEquals(p.getNetWeight(),25, 0.1);
        Assert.assertEquals(p.getGrossWeight(),30, 0.1);
    }

    @Test
    public void gettersAndSetters(){
        PackagedPieceProduct p = new PackagedPieceProduct(new PieceProduct("name", "description", 5), 5, new Pack("name",5));
        Assert.assertEquals(p.getQuantity(),5);
        p.setQuantity(2);
        Assert.assertEquals(p.getQuantity(),2);

        Assert.assertEquals(p.getPack(),new Pack("name",5));
        p.setPack(new Pack("name",3));
        Assert.assertEquals(p.getPack(),new Pack("name", 3));
    }

    @Test
    public void equals(){
        PackagedPieceProduct p1 = new PackagedPieceProduct(new PieceProduct("name", "description", 5),5, new Pack("name", 5));
        PackagedPieceProduct p2 = new PackagedPieceProduct(new PieceProduct("name", "description", 5), 6, new Pack("name", 5));
        PackagedPieceProduct p3 = new PackagedPieceProduct(new PieceProduct("name", "description", 5), 5, new Pack("name", 6));
        PackagedPieceProduct p4 = new PackagedPieceProduct(new PieceProduct("not the same name", "description", 5), 5, new Pack("name", 5));
        PackagedPieceProduct p5 = new PackagedPieceProduct(new PieceProduct("name", "not the same description", 5), 5, new Pack("name", 5));
        Assert.assertNotEquals(p1,p2);
        Assert.assertNotEquals(p1,p2);
        Assert.assertNotEquals(p1,p3);
        Assert.assertNotEquals(p1,p4);
        Assert.assertNotEquals(p1,(PackagedPieceProduct)null);


    }
}