package Tests.Task3;


import Task1.*;
import Task2.BeginStringFilter;
import Task3.ProductService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductServiceTest {
    private static PackedProduct[] packedProducts = new PackedProduct[]{
             new PackagedWeightProduct(new WeightProduct("name 2","description 2"),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 2","description 2"),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 3","description 3"),5, new Pack("name3", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 4","description 4"),5, new Pack("name4", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 5","description 5"),5, new Pack("name5", 5))
    };

    private static PackedProduct[] packedProducts2 = new PackedProduct[]{new PackagedPieceProduct(
             new PieceProduct("name 2","description 2",1),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 2","description 2"),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 3","description 3"),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 4","description 4"),5, new Pack("name2", 5))
            ,new PackagedWeightProduct(new WeightProduct("name 5","description 5"),5, new Pack("name2", 5))
    };

    private static Consignment packedProductsBach = new Consignment(packedProducts,"description");

    private static PackagedSetProduct PackagedSetProduct = new PackagedSetProduct(packedProducts,new Pack("name", 5));

    private static Consignment pb = new Consignment(new PackedProduct[]{PackagedSetProduct,PackagedSetProduct},"description 2");

    private static Consignment pb2 = new Consignment(new PackedProduct[]{PackagedSetProduct,new PackagedWeightProduct(
            new WeightProduct("name 2","description 2"),5,new Pack("name2",5))},"description");

    private static Consignment pb3 = new Consignment(new PackedProduct[]{PackagedSetProduct,PackagedSetProduct,null},"description 2");

    @Test
    public void countByFilter(){
        Assert.assertEquals(ProductService.countByFilter(new BeginStringFilter("name"),packedProductsBach),5);
        Assert.assertEquals(ProductService.countByFilter(new BeginStringFilter("name"), pb),0);
        Assert.assertEquals(ProductService.countByFilter(new BeginStringFilter("name"),pb2),1);
    }

    @Test
    public void countByFilterDeep(){
        Assert.assertEquals(ProductService.countByFilterDeep(new BeginStringFilter("name"), pb),10);
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> ProductService.countByFilterDeep(new BeginStringFilter("name"), null)));
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> ProductService.countByFilterDeep(null, pb)));
        Assert.assertEquals(ProductService.countByFilterDeep(new BeginStringFilter("name"),pb3),10);
    }

    @Test
    public void checkAllWeighted(){
        Assert.assertTrue(ProductService.checkAllWeighted(packedProductsBach));
        Assert.assertFalse(ProductService.checkAllWeighted(new Consignment(packedProducts2,"description")));
    }
}