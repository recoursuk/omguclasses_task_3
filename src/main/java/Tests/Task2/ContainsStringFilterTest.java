package Tests.Task2;

import Task2.ContainsStringFilter;
import Task2.Filter;
import org.junit.Assert;
import org.junit.Test;

public class ContainsStringFilterTest {

    @Test
    public void Apply(){
        Filter f = new ContainsStringFilter("test");
        Assert.assertTrue(f.apply("not testtest"));
        Assert.assertFalse(f.apply("not est"));
    }
}