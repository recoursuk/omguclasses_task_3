package Tests.Task2;

import Task2.BeginStringFilter;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class BeginStringFilterTest {
    @Test
    public void constructor(){
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> new BeginStringFilter("")));
        Assert.assertNotNull(assertThrows(NullPointerException.class,() -> new BeginStringFilter(null)));

    }

    @Test
    public void testApply(){
        BeginStringFilter b = new BeginStringFilter("test");
        Assert.assertTrue(b.apply("test message"));
        Assert.assertFalse(b.apply("gtest message"));
    }
}