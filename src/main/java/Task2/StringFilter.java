package Task2;

abstract public class StringFilter implements Filter{
    private String pattern;
    public StringFilter(String pattern){
        if (pattern == null || "".equals(pattern)){
            throw new NullPointerException("String Can't be null");
        }
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        if (pattern == null || "".equals(pattern)) {
            throw new IllegalArgumentException("String can't be null");
        } else {
            this.pattern = pattern;
        }
    }
}
