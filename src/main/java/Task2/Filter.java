package Task2;

public interface Filter {
    boolean apply(String string);
}
