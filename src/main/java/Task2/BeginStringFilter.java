package Task2;

public class BeginStringFilter extends StringFilter implements Filter {
    public BeginStringFilter(String pattern){
        super(pattern);
    }

    @Override
    public boolean apply(String pattern){
        return pattern.startsWith(super.getPattern());
    }
}
