package Task2;

public class EndStringFilter extends StringFilter implements Filter {
    public EndStringFilter(String pattern){
        super(pattern);
    }

    @Override
    public boolean apply(String pattern){
        return pattern.endsWith(super.getPattern());
    }
}
