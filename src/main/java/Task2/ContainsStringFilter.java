package Task2;

import java.util.regex.Pattern;

public class ContainsStringFilter extends StringFilter implements Filter{
    public ContainsStringFilter(String pattern){
        super(pattern);
    }

    @Override
    public boolean apply(String pattern){
        return Pattern.compile(super.getPattern()).matcher(pattern).find();
    }
}
